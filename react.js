import reactPlugin from "eslint-plugin-react";

export default [
	reactPlugin.configs.flat.recommended,
	reactPlugin.configs.flat["jsx-runtime"],
	{
		rules: {
			"react/no-unescaped-entities": "warn",
			"react/jsx-key": "off"
		}
	}
];
