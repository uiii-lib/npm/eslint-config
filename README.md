# @uiii-lib eslint config for Typescript projects

It offers three configs: `base`, `react` and `emotion`.

## Usage

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install

```
npm install --save-dev eslint prettier @uiii-lib/eslint-config
```

And put into `eslint.config.js`

```js
import eslintConfig from "@uiii-lib/eslint-config";

export default [
	eslintConfig
];
```

If the project is in a subdirectory, add this to `.eslintrc.js`

```js
export default [
	{
		...
		languageOptions: {
			parserOptions: {
				tsconfigDirName: import.meta.dirname
			}
		}
	}
];
```

### React

To use config for React, install dependecies first

```
npm install --save-dev eslint-plugin-react eslint-plugin-react-hooks
```

Then extend the config in `eslint.config.js`

```js
import eslintConfig from "@uiii-lib/eslint-config";
import reactConfig from "@uiii-lib/eslint-config/react";

export default [
	eslintConfig,
	reactConfig
];
```

### Emotion

To use config for [Emotion](https://emotion.sh), setup [React](#react) first, then install dependecies

```
npm install --save-dev @emotion/eslint-plugin
```

Now extend the config in `eslint.config.js`

```js
import eslintConfig from "@uiii-lib/eslint-config";
import reactConfig from "@uiii-lib/eslint-config/react";
import emotionConfig from "@uiii-lib/eslint-config/emotion";

export default [
	{
		eslintConfig,
		reactConfig,
		emotionConfig
		...
	}
];
```

If you are using [Create React App](https://create-react-app.dev) add the following to `eslint.config.js`

```js
export default [
	{
		...
		rules: {
			...
			"@emotion/jsx-import": ["error", {
				"runtime": "automatic"
			}]
		}
	}
];
```

### Tailwind CSS

To use config for [Tailwind CSS](https://tailwindcss.com), install dependencies first

```
npm install --save-dev eslint-plugin-tailwindcss
```

Then extend the config in `eslint.config.js`

```js
import eslintConfig from "@uiii-lib/eslint-config";
import tailwindConfig from "@uiii-lib/eslint-config/tailwind";

export default [
	eslintConfig,
	tailwindConfig
];
```

## Notes

> For more option how to use visit https://eslint.org/docs/latest/extend/shareable-configs

It is recommended to also install [@uiii-lib/prettier-config](https://gitlab.com/uiii-lib/prettier-config)
