import tailwindPlugin from "eslint-plugin-tailwindcss";

export default [
	...tailwindPlugin.configs["flat/recommended"]
]
