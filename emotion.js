import emotionPlugin from "@emotion/eslint-plugin";

export default [
	{
		plugins: {
			"@emotion": emotionPlugin
		},
		rules: {
			"react/no-unknown-property": ["error", {
				"ignore": ["css"]
			}]
		}
	}
]
